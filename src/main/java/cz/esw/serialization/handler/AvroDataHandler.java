package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.avro.*;
import cz.esw.serialization.json.DataType;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Marek Cuchý (CVUT)
 */
public class AvroDataHandler implements DataHandler {

    private final InputStream is;
    private final OutputStream os;

    protected Map<Integer, ADataset> datasets;

    /**
     * @param is input stream from which the results will be read
     * @param os output stream to which the data have to written
     */
    public AvroDataHandler(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

    @Override
    public void initialize() {
        datasets = new HashMap<>();
    }

    @Override
    public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
        ADataset aDataset = new ADataset();
        aDataset.setInfo(new AMeasurementInfo(datasetId, timestamp, measurerName));
        aDataset.setRecords(new HashMap<>());
        datasets.put(datasetId, aDataset);
    }

    @Override
    public void handleValue(int datasetId, DataType type, double value) {
        ADataset aDataset = datasets.get(datasetId);
        if (aDataset == null) {
            throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
        }
        aDataset.getRecords().computeIfAbsent(type.toString(), t -> new ArrayList<>()).add(value);
    }

    @Override
    public void getResults(ResultConsumer consumer) throws IOException {
        DatumWriter<ADatasets> datumWriter = new SpecificDatumWriter<>(ADatasets.class);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(byteArrayOutputStream, null);
        final ADatasets datasetsWrapper = new ADatasets();
        datasetsWrapper.setDatasets(new ArrayList<>(datasets.values().size()));
        for (ADataset dataset : datasets.values().toArray(new ADataset[]{})) {
            datasetsWrapper.getDatasets().add(dataset);
        }
        datumWriter.write(datasetsWrapper, encoder);
        encoder.flush();
        int serializedSize = byteArrayOutputStream.size();
        final byte[] sizeBytes = new byte[]{
                (byte) ((serializedSize) & 0xFF),
                (byte) ((serializedSize >>> 8) & 0xFF),
                (byte) ((serializedSize >>> 16) & 0xFF),
                (byte) ((serializedSize >>> 24) & 0xFF),
        };
        this.os.write(sizeBytes);
        this.os.flush();
        byteArrayOutputStream.writeTo(os);

        DatumReader<AResults> resultReader = new SpecificDatumReader<>(AResults.class);
        final BinaryDecoder binaryDecoder = DecoderFactory.get().binaryDecoder(is, null);
        AResults results = resultReader.read(null, binaryDecoder);

        for (AResult singleResult : results.getResults()) {
            AMeasurementInfo info = singleResult.getInfo();
            consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName().toString());
            Map<String, Double> averagesMap = singleResult.getAverages();
            consumer.acceptResult(DataType.DOWNLOAD, averagesMap.get(DataType.DOWNLOAD.toString()));
            consumer.acceptResult(DataType.UPLOAD, averagesMap.get(DataType.UPLOAD.toString()));
            consumer.acceptResult(DataType.PING, averagesMap.get(DataType.PING.toString()));
        }

    }
}
