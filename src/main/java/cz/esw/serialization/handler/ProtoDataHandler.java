package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.proto.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Marek Cuchý (CVUT)
 */
public class ProtoDataHandler implements DataHandler {

	private final InputStream is;
	private final OutputStream os;
	protected Map<Integer, PDataset> datasets;

	/**
	 * @param is input stream from which the results will be read
	 * @param os output stream to which the data have to written
	 */
	public ProtoDataHandler(InputStream is, OutputStream os) {
		this.is = is;
		this.os = os;
	}

	@Override
	public void initialize() {
		datasets = new HashMap<>();
	}

	@Override
	public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
		PDataset.Builder datasetBuilder = PDataset.newBuilder();

		PMeasurementInfo.Builder pMeasurementInfoBuilder =
				PMeasurementInfo.newBuilder()
						.setId(datasetId)
						.setTimestamp(timestamp)
						.setMeasurerName(measurerName);

		datasetBuilder.setInfo(pMeasurementInfoBuilder);

		//records is initialised automatically when adding a value

		datasets.put(datasetId, datasetBuilder.build());
	}


	@Override
	public void handleValue(int datasetId, DataType type, double value) {
		String dataType = type.toString();
		PDataset dataset = datasets.get(datasetId);
		if (dataset == null) {
			throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
		}

		PDataSetValues recordsOrDefault = dataset.getRecordsOrDefault(dataType, PDataSetValues.newBuilder().build());
		PDataset.Builder builder = dataset.toBuilder().putRecords(dataType, recordsOrDefault.toBuilder().addValues(value).build());
		datasets.put(datasetId, builder.build());
	}

	@Override
	public void getResults(ResultConsumer consumer) throws IOException {
		PDatasets message = PDatasets.newBuilder().putAllDatasets(datasets).build();
		final int serializedSize = message.getSerializedSize();
		final byte[] sizeBytes = new byte[]{
				(byte) ((serializedSize) & 0xFF),
				(byte) ((serializedSize >>> 8) & 0xFF),
				(byte) ((serializedSize >>> 16) & 0xFF),
				(byte) ((serializedSize >>> 24) & 0xFF),
		};
		this.os.write(sizeBytes);
		message.writeTo(os);

		PResults pResults = PResults.parseFrom(is);

		for (PResult result : pResults.getResultList()) {
			PMeasurementInfo info = result.getInfo();
			consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName());
			Map<String, Double> averagesMap = result.getAveragesMap();
			consumer.acceptResult(DataType.DOWNLOAD, averagesMap.get(DataType.DOWNLOAD.toString()));
			consumer.acceptResult(DataType.UPLOAD, averagesMap.get(DataType.UPLOAD.toString()));
			consumer.acceptResult(DataType.PING, averagesMap.get(DataType.PING.toString()));
		}
	}
}
