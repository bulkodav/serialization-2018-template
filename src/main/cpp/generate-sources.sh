echo "creating folders structure ..."
mkdir generated
mkdir generated/avro
mkdir generated/protobuf
echo "generating avro classes ..."
avrogencpp -i ../avro/measurements.avsc -o generated/avro/measurements.hh -n eswavro
echo "generating protobuf classes ..."
protoc -I=../proto --cpp_out=generated/protobuf/ ../proto/measurements.proto