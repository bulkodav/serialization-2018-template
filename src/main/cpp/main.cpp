#include <iostream>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <cstring>
#include <unistd.h>


#include <jsoncpp/json/json.h>


#include "measurementinfo.h"
#include "dataset.h"
#include "result.h"

//#include <boost/array.hpp>
#include <boost/asio.hpp>

#include <functional>
#include "avro/Encoder.hh"
#include "avro/Decoder.hh"
 

#include "generated/protobuf/measurements.pb.h"
#include "generated/avro/measurements.hh"

using namespace std;
using boost::asio::ip::tcp;

void processJSON(tcp::iostream& stream){
    Json::Value val;
    Json::Reader reader;

    std::vector<Dataset> datasets;
    std::vector<Result> results;

    /* Read json string from the stream */
    string s;
    getline(stream, s, '\0');

    /* Parse string */
    reader.parse(s, val);

    datasets.clear();
    results.clear();
    for (int i = 0; i < val.size(); i++) {
        datasets.emplace_back();
        datasets[i].Deserialize(val[i]);
        /* Calculate averages */
        results.emplace_back(datasets[i].getInfo(), datasets[i].getRecords());
    }

    /* Create output JSON structure */
    Json::Value out;
//    Json::FastWriter writer;
    Json::StyledWriter writer;
    for (int i = 0; i < results.size(); i++) {
        Json::Value result;
        results[i].Serialize(result);
        out[i] = result;
    }

    /* Send the result back */
    std::string output = writer.write(out);
    stream << output;
    cout << output;
}

void processAvro(tcp::iostream& stream){
    char size_buffer[4];
    stream.read(size_buffer, 4);
    int size;
    std::memcpy(&size, size_buffer, sizeof(int));
    char *buffer = new char[size];
    stream.read(buffer, size);
    std::unique_ptr<avro::InputStream> in = avro::memoryInputStream((const uint8_t*)buffer, size);
    avro::DecoderPtr d = avro::binaryDecoder();
    d->init(*in);
    eswavro::ADatasets message;
    avro::decode(*d, message);
    delete[] buffer;
    eswavro::AResults results;
    for(const auto& dataset : message.datasets) {
        eswavro::AResult result;
        result.info.id = dataset.info.id;
        result.info.timestamp = dataset.info.timestamp;
        result.info.measurerName = dataset.info.measurerName;
        const auto averages = &result.averages;
        for(const auto& record : dataset.records) {
            double number_of_measurements = record.second.size();
            int sum = 0;
            for(int index = 0; index < number_of_measurements; ++index) {
                sum += record.second[index];
            }
            (*averages)[record.first] = sum/number_of_measurements;
        }
        results.results.push_back(std::move(result));
    }
    std::unique_ptr<avro::OutputStream> out = avro::memoryOutputStream();
    avro::EncoderPtr e = avro::binaryEncoder();
    e->init(*out);
    avro::encode(*e, results);
    const auto snapshot = avro::snapshot(*out);
    stream.write(reinterpret_cast<const char*>(snapshot->data()), snapshot->size());
}

void processProtobuf(tcp::iostream& stream){
    char size_buffer[4];
    stream.read(size_buffer, 4);
    int size;
    std::memcpy(&size, size_buffer, sizeof(int));
    esw::PDatasets message;
    char *buffer = new char[size];
    stream.read(buffer, size);
    bool parsed = message.ParseFromArray(buffer, size);
    delete[] buffer;
    if(!parsed) {
        std:cerr << "Failed to parse the message." << std::endl;
    }
    esw::PResults results;
    for(const auto& kv : message.datasets()) {
        auto dataset = kv.second;
        auto result = results.add_result();
        auto info = result->mutable_info();
        info->set_id(kv.first);
        info->set_timestamp(dataset.info().timestamp());
        info->set_measurername(dataset.info().measurername());

        auto averages = result->mutable_averages();
        for(const auto& record : dataset.records()) {
            double number_of_measurements = record.second.values_size();
            int sum = 0;
            for(int index = 0; index < number_of_measurements; ++index) {
                sum += record.second.values(index);
            }
            (*averages)[record.first] = sum/number_of_measurements;
        }
    }
    int response_size = results.ByteSize();
    char* response_buffer = new char[response_size];
    results.SerializeToArray(response_buffer, response_size);
    stream.write(response_buffer, response_size);


}

int main(int argc, char *argv[]) {

    if (argc != 3) {
        cout << "Error: two arguments required - ./server  <port> <protocol>" << endl;
        return 1;
    }



    // unsigned short int port = 12345;
    unsigned short int port = atoi(argv[1]);

    // std::string protocol = "json";
    std::string protocol(argv[2]);
    try {
        boost::asio::io_service io_service;

        tcp::endpoint endpoint(tcp::v4(), port);
        tcp::acceptor acceptor(io_service, endpoint);

        while (true) {
            tcp::iostream stream;
            boost::system::error_code ec;
            acceptor.accept(*stream.rdbuf(), ec);

            if(protocol == "json"){
                processJSON(stream);
            }else if(protocol == "avro"){
                processAvro(stream);
            }else if(protocol == "proto"){
                processProtobuf(stream);
            }else{
                throw std::logic_error("Protocol not yet implemented");
            }

        }

    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
